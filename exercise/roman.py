from const import ROMAN_NUMBER_DICT

class romanLetter:
    letter = ""
    
    def __init__(self, letter):
        self.letter = letter
    
    def getLetter(self):
        return self.letter
    

 
    
def compare(first, other):
    if ROMAN_NUMBER_DICT[first.letter] < ROMAN_NUMBER_DICT[other.letter]:
        return 1
    elif ROMAN_NUMBER_DICT[first.letter] > ROMAN_NUMBER_DICT[other.letter]:
        return -1
    else:
        return 0
    
class romanLetterList:
    letterList=[]
    
    def __init__(self, letterList):
        self.letterList=letterList
    

    def _romanNumberListSort(self):
        lettList = self.letterList
        lettList.sort(cmp=compare)
        self.letterList = lettList
    
    def _romanNumberListClean(self):
        isClean = False
        while not(isClean):
            isClean, self.letterList = self._applyCleaning(self.letterList)
        return self.letterList
    
    def _romanElementListToString(self):
        romanString=''
        return roman(romanString.join([letter.getLetter() for letter in self.letterList]))
        
class roman:
    number = ""
    def replace_last(self, replace_what, replace_with):
        head, sep, tail = self.number.rpartition(replace_what)
        self.number = head + replace_with + tail
    
    def __init__(self, number):
        self.number= number
    
    def getNumber(self):
        return self.number
    
    def __add__(self, other):
        sum= romanSum(self.number, other.number)
        return sum.makeSum()

    def _lastDominantCharacter(self):
        if len(self.number) > 1 and ROMAN_NUMBER_DICT[self.number[-1]] > ROMAN_NUMBER_DICT[self.number[-2]]:
            return self.number[-2]+self.number[-1]
        else:
            return self.number[-1]
 
    def _numberPartition(self):
        """
        return two string: the first contains the sum elements, the second the subtracting elements
        """
        romanNumber=self.number
        sumLetter=[]
        subtractLetter=[]
        if len(romanNumber) == 1:
            sumLetter.append(romanLetter(romanNumber[0]))
        else:
            for letterIdx in range(0,len(romanNumber)):
                if letterIdx < len(romanNumber)-1 and ROMAN_NUMBER_DICT[romanNumber[letterIdx]] < ROMAN_NUMBER_DICT[romanNumber[letterIdx+1]]:
                    subtractLetter.append(romanLetter(romanNumber[letterIdx]))
                else:
                    sumLetter.append(romanLetter(romanNumber[letterIdx]))
        return romanLetterList(sumLetter), romanLetterList(subtractLetter)
    
    def _stringToRomanElementList(self):
        return romanLetterList([romanLetter(character) for character in self.number])
    
    def _checkClean(self):
        if (self.number.count('I') > 3 or
            self.number.count('V') > 1 or
            self.number.count('X') > 3 or
            self.number.count('L') > 1 or
            self.number.count('C') > 3 or
            self.number.count('D') > 1):
            return False
        else:
            return True
        
    def _sumIteration(self):
        while self._checkClean() == False:
            if 'I'*6 in self.number:
                self.number = self.number.replace('I'*6, 'VI')
            if 'I'*5 in self.number:
                self.number = self.number.replace('I'*5, 'V')
            if 'I'*4 in self.number:
                self.number = self.number.replace('I'*4, 'IV')  
            if 'V'*2 in self.number:
                self.number = self.number.replace('V'*2, 'X')  
            if 'X'*6 in self.number:
                self.number = self.number.replace('X'*6, 'LX')
            if 'X'*5 in self.number:
                self.number = self.number.replace('X'*5, 'L')
            if 'X'*4 in self.number:
                self.number = self.number.replace('X'*4, 'XL')        
            if 'L'*2 in self.number:
                self.number = self.number.replace('L'*2, 'L')   
            if 'C'*6 in self.number:
                self.number = self.number.replace('C'*6, 'DC')
            if 'C'*5 in self.number:
                self.number = self.number.replace('C'*5, 'D')
            if 'C'*4 in self.number:
                self.number = self.number.replace('C'*4, 'CD') 
            if 'D'*2 in self.number:
                self.number = self.number.replace('D'*2, 'M')  
    
    def _subIteration(self,other):
        while other.number != '':
            element = other.number[0]
            #Subtract by I
            if (element =='I'and 'I' in self.number):
                if ('IV' not in self.number and 'IX' not in self.number):
                    self.number= self.number.replace('I', '', 1)
                elif 'IV' in self.number:
                    self.replace_last('IV', 'III')
                elif 'IX' in self.number:
                    self.replace_last('IX', 'VIII')
            elif (element == 'I' and 'I' not in self.number):
                if 'V'in self.number:
                    self.replace_last('V', 'IV')
                elif 'X' in self.number:
                    self.replace_last('X', 'IX')
                elif 'L' in self.number:
                    self.replace_last('L', 'XLIX')
                elif 'C' in self.number:
                    self.replace_last('C', 'XCIX')
                elif 'D' in self.number:
                    self.replace_last('C', 'XDIX')
                elif 'M' in self.number:
                    self.replace_last('M','CMXCIX')
            #subtract by X 
            elif (element =='X'and 'X' in self.number):
                if ('XC' not in self.number and 'XL' not in self.number and 'IX' not in self.number):
                    self.number= self.number.replace('X', '', 1)
                else:
                    if 'IX' in self.number:
                        self.number= self.number.replace('IX', '',1)
                        other.number = other.number + 'I'
                    elif 'XC' in self.number:
                        self.number= self.number.replace('XC', 'LXXX',1)
                    elif 'XL' in self.number:
                        self.number= self.number.replace('XL', 'XXX',1)
            elif (element == 'X' and 'X' not in self.number):
                if 'L' in self.number:
                    self.replace_last('L', 'XL')
                elif 'C' in self.number:
                    self.replace_last('C', 'XC')
                elif 'D' in self.number:
                    self.replace_last('C', 'CDXC')
                elif 'M' in self.number:
                    self.replace_last('M','CMXC',1)
            #subtract by C
            elif (element =='C'and 'C' in self.number):
                if ('XC' not in self.number and 'CM' not in self.number and 'CD' not in self.number):
                    self.number= self.number.replace('C', '', 1)
                else:
                    if 'XC' in self.number:
                        self.number= self.number.replace('XC', '',1)
                        other.number = other.number + 'X'
                    else:
                        self.number= self.number.replace('CM', 'DCCC',1)
                        self.number= self.number.replace('CD', 'CCC',1)
            elif (element == 'C' and 'C' not in self.number):
                if 'D' in self.number:
                    self.replace_last('C', 'CD')
                elif 'M' in self.number:
                    self.replace_last('M','CM')
            other.number= other.number.replace(element, '',1)
                 

        
#     def _applyCleaning(self, letterString):
#         if ('IIII' in letterString):
class romanSum:
    firstNumber= roman('')
    secondNumber= roman('')
    def __init__(self, firstNumber, secondNumber):
        self.firstNumber =  roman(firstNumber)
        self.secondNumber =  roman(secondNumber)        
    
    
    def makeSum(self):
        sumLetter, subtractLetter = self._generateNumbersPartitions()
        sumLetter._romanNumberListSort()
        sumLetterRoman= sumLetter._romanElementListToString()
        sumLetterRoman._sumIteration()
        subtractLetterRoman = subtractLetter._romanElementListToString()
        sumLetterRoman._subIteration(subtractLetterRoman)
        return sumLetterRoman.number

    
    def _generateNumbersPartitions(self):
        sumLetter=[]
        subtractLetter=[]
        sumLetterNumber, subtractLetterNumber = self.firstNumber._numberPartition()
        sumLetter.extend(sumLetterNumber.letterList)
        subtractLetter.extend(subtractLetterNumber.letterList)
        sumLetterNumberOther, subtractLetterNumberOther = self.secondNumber._numberPartition()
        sumLetter.extend(sumLetterNumberOther.letterList)
        subtractLetter.extend(subtractLetterNumberOther.letterList)
        return romanLetterList(sumLetter), romanLetterList(subtractLetter)
        
        